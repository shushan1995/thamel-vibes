import product1 from '../../images/2.jpg';
import product2 from '../../images/3.jpg';
import product3 from '../../images/6.jpg';
import product4 from '../../images/mousse.jpg';
import product5 from '../../images/waffles.jpg';
import product6 from '../../images/donut.jpg';

export const productData = [
    {
        img: product1,
        alt: 'Burger',
        name: 'Hamburger',
        desc: 'Will think about later for description of this food',
        price: 'Rs.300',
        button: 'Add to Cart'
    },
    {
        img: product2,
        alt: 'Hotdog',
        name: 'American Hotdog',
        desc: 'Will think about later for description of this food',
        price: 'Rs.250',
        button: 'Add to Cart'
    },
    {
        img: product3,
        alt: 'Pizza',
        name: 'Cheese Pizza',
        desc: 'Will think about later for description of this food',
        price: 'Rs.350',
        button: 'Add to Cart'
    },
]

export const productData2 = [
    {
        img: product4,
        alt: 'Desert',
        name: 'Chocolate Mousse',
        desc: 'Will think about later for description of this food',
        price: 'Rs.200',
        button: 'Add to Cart'
    },
    {
        img: product5,
        alt: 'Desert',
        name: 'Belgium Waffles',
        desc: 'Will think about later for description of this food',
        price: 'Rs.150',
        button: 'Add to Cart'
    },
    {
        img: product6,
        alt: 'Desert',
        name: 'Chocolate Donut',
        desc: 'Will think about later for description of this food',
        price: 'Rs.90',
        button: 'Add to Cart'
    },
]